#include <iostream>

using namespace std;

int main ()
{
    
    int a, b;
    
    string numbers [11] = {"even", "odd", "one", "two", "three", 
    "four", "five", "six", "seven", "eight", "nine"};

    cin >> a >> b;

    for (int i = a; i <= b; i++)
    {
        if ((i>9) && (i%2 == 0))
        {
            cout << numbers[0] << endl;
        }
        else if ((i>9) && (i%2 != 0))
        {
            cout << numbers[1] << endl;
        }
        else
        {
            cout << numbers[i+1] << endl;
        }
    }
    return 0;
}